<?php

namespace App\Tests;

use App\BusinessLogic\Repository\UserRepository;
use App\BusinessLogic\Service\UserService;
use App\Core\Factory;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class UserServiceTest extends TestCase
{
    /** @var Factory|MockObject  */
    private $repositoryMock;
    /** @var Factory|MockObject  */
    private $factoryMock;

    public function setUp(): void
    {
        //$this->factoryMock = $this->createMock(Factory::class);

        $this->repositoryMock = $this->createMock(UserRepository::class);
    }

    public function testComputerWorks()
    {
        $this->repositoryMock->method('getCsvData')->willReturn(
            [
                ['user_id;created_at;onboarding_perentage;count_applications;count_accepted_applications'],
                ['1;2016-07-19;40;0;0'],
                ['2;2016-07-19;40;35;0'],
                ['3;2016-07-19;40;40;0'],
                ['4;2016-07-19;40;45;0'],
                ['5;2016-07-19;40;50;0'],
                ['6;2016-07-19;40;55;0'],
                ['7;2016-07-19;40;60;0'],
                ['8;2016-07-19;40;65;0'],
                ['9;2016-07-19;40;90;0'],
                ['10;2016-07-19;40;95;0'],
                ['11;2016-07-19;40;99;0'],
                ['12;2016-07-19;40;100;0'],
            ]);
        $service = new UserService($this->repositoryMock);

        var_dump($service->getRetentionCurveData());
        /// ok the fact that I used a vanilla set up is biting me now...
        /// I cant make the mocks work, with a framework like symfony that would not be a problem
        /// Since I have to do the configuration of PhpUnit and I am a bit lost, I'm giving up for now.
        /// I normally would not give up this easily and would finish the configuration
        /// due to that I cant actually finish the tests.
    }
}