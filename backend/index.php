<?php

namespace App;

use App\Core\Router;
use App\Core\Response;
use App\Core\Configuration;
use App\Core\Psr4Autoloader;
use App\Core\DependencyInjection;

/**
 * Class App
 * @package App
 */
class App
{
    const INSTALLATION_BASE_DIR = '.';
    const APP_NAMESPACE = 'App';
    const CLASS_FILE_AUTOLOADER = 'Core/Psr4Autoloader.php';

    private $configuration;

    public function run()
    {
        // got auto loader from http://www.php-fig.org/psr/psr-4/examples/
        /** @noinspection PhpIncludeInspection */
        require_once self::CLASS_FILE_AUTOLOADER;
        $loader = new Psr4Autoloader;
        $loader->register();
        $loader->addNamespace(self::APP_NAMESPACE, self::INSTALLATION_BASE_DIR);

        $this->configuration = new Configuration;

        $route = new Router($this->configuration->get('router'));
        $dependencyInjection = new DependencyInjection($route);

        $response = $this->call(
            $dependencyInjection->getController(),
            $dependencyInjection->getAction(),
            $dependencyInjection->getParameters());

        $response->render();
    }

    private function call($controller, $action, $params): Response
    {
        return call_user_func_array([$controller, $action], $params);
    }
}

header("Access-Control-Allow-Origin: http://localhost:81");
header('Access-Control-Allow-Methods: POST, GET, PUT, DELETE, OPTIONS');
header('Access-Control-Allow-Headers: X-PINGARUNER');
header('Content-type: text/plain');

function dump($values) {
    echo "<pre>\n";
    var_dump($values);
}

function dd($values) {
    dump($values);
    die;
}

(new App())->run();