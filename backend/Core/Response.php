<?php


namespace App\Core;


class Response
{
    const TYPE_JSON = 'text/json';
    protected $type;
    protected $content;
    protected $responseCode;

    function __construct(array $content, string $type, $code = 200) {
        $this->type         = $type;
        $this->content      = $content;
        $this->responseCode = $code;
    }

    public function render()
    {
        http_response_code($this->responseCode);
        header('Content-Type: '.$this->type);
        switch ($this->type) {
            // add type of responses as needed
            case self::TYPE_JSON:
                $this->renderJson();
                break;

            default:
                # code...
                break;
        }
    }

    private function renderJson()
    {
        echo json_encode($this->content);
    }

}