<?php


namespace App\Core;


use ReflectionClass;
use ReflectionMethod;

class DependencyInjection
{
    private $route;
    private $controller;
    private $action;
    private $params;
    const CONTROLLER_NAMESPACE = 'App\BusinessLogic\Controller\\';
    const CONSTRUCT_METHOD_NAME = '__construct';

    function __construct(Router $route)
    {
        $this->route = $route;
        $this->init();
    }

    private function init()
    {
        // Get target controller and action from the route
        $target = explode(':', $this->route->getTarget());
        $targetController = self::CONTROLLER_NAMESPACE.$target[0];

        $this->controller = $this->getObject($targetController);
        $this->action = $target[1];

        // Reflect upon the action we are going to execute
        $methodReflection = new ReflectionMethod($targetController, $this->action);

        // Get its parameters
        $methodParams = $methodReflection->getParameters();

        // resolve the parameters
        $params = $this->resolve($methodParams);

        // set the route parameters that we already got from the route
        $this->params = $this->route->getParams();

        // Set the Params needed for the action that are not from the route
        foreach ($params as $param => $value) {
            $this->params[$param] = $value;
        }
    }


    private function resolve($reflectionParameters)
    {
        $resolvedParams = [];
        foreach ($reflectionParameters as $parameter) {

            if (!$this->isRouteParameter($parameter->getName())) {
                $class = $parameter->getClass();

                $obj = $this->getObject($class->getName());

                $resolvedParams[$parameter->getName()] = $obj;
            }
        }
        return $resolvedParams;
    }

    private function isRouteParameter($name)
    {
        return isset($this->route->getParams()['{'.$name.'}']);
    }

    private function getObject($name)
    {
        if (class_exists($name)) {
            $parameters = [];
            if (method_exists($name, self::CONSTRUCT_METHOD_NAME)) {
                $reflection = new ReflectionMethod($name, self::CONSTRUCT_METHOD_NAME);

                $reflectionParams = $reflection->getParameters();

                $parameters = $this->resolve($reflectionParams);
            }

            $reflectionObject = new ReflectionClass($name);

            return $reflectionObject->newInstanceArgs($parameters);
        }
    }

    public function getController()
    {
        return $this->controller;
    }

    public function getAction()
    {
        return $this->action;
    }

    public function getParameters()
    {
        return $this->params === null ? [] : $this->params;
    }
}
