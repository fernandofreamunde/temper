<?php
namespace App\Core;

use function App\dd;

/**
 * Class Configuration
 * @package App\Core
 */
class Configuration
{
    const CONFIGURATION_DIRECTORY_NAME = '/Config/';
    const CORE_DIRECTORY_NAME = '/Core';
    const DATA_DIRECTORY_NAME = '/Data';
    const CONFIGURATION_FILE_JSON_EXTENSION = '*.json';

    private $configuration;
    private $baseDirectory;
    private $configurationDirectory;

    function __construct()
    {
        $this->baseDirectory = str_replace(self::CORE_DIRECTORY_NAME, '', __DIR__);
        $this->configurationDirectory = $this->baseDirectory . self::CONFIGURATION_DIRECTORY_NAME;

        $configs = glob($this->configurationDirectory . self::CONFIGURATION_FILE_JSON_EXTENSION);

        foreach($configs as $path)
        {
            $filename = str_replace($this->configurationDirectory, '', $path);

            $configurationType = explode('.', $filename)[0];

            $this->configuration[$configurationType] = json_decode(file_get_contents($path), true);
        }
    }

    public function get($value)
    {
        return $this->configuration[$value];
    }

    /**
     * @return mixed
     */
    public function getDataDirectory()
    {
        return $this->baseDirectory.self::DATA_DIRECTORY_NAME;
    }
}
