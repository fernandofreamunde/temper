<?php


namespace App\Core;


class JsonResponse extends Response
{
    public function __construct(array $content, $code = 200)
    {
        parent::__construct($content, self::TYPE_JSON, $code);
        $this->contentToArray();
    }

    private function contentToArray():array
    {
        $toReturn = [];
        foreach ($this->content as $key => $content) {
            if (is_array($content)) {
                foreach ($content as $contentItem) {
                    if (is_object($contentItem)) {
                        $toReturn[$key][] = $this->objectToArray($contentItem);
                    }
                }
            } elseif (is_numeric($key)) {
                if (is_object($content)) {
                    $toReturn[] = $this->objectToArray($content);
                }
            } elseif (is_object($content)) {
                $toReturn[$key] = $this->objectToArray($content);
            } else {
                $toReturn[$key] = $content;
            }
        }

        return $toReturn;
    }

    private function objectToArray($object)
    {
        $ref = new ReflectionClass($object);
        //var_dump($object);die;

        $methods = [];
        foreach ($ref->getMethods() as $method) {
            if ($method->getName() === '__construct') { continue; }
            $methods[strtolower(str_replace('_', '', $method->getName()))] = $method->getName();
        }

        $propertyMethodMap = [];
        foreach ($ref->getProperties() as $var) {
            $propertyMethodMap[$var->getName()] = $methods['get'.str_replace('_', '',$var->getName())];
        }

        $objectArray = [];
        foreach ($propertyMethodMap as $key => $value) {
            $reflectionMethod = new ReflectionMethod($ref->getName(), $value);

            if (is_object($reflectionMethod->invoke($object))) {
                $objectArray[$key] = $this->objectToArray($reflectionMethod->invoke($object));
            } elseif (is_array($reflectionMethod->invoke($object))) {
                $subArr = [];
                foreach ($reflectionMethod->invoke($object) as $subValue) {
                    $subArr[] = $this->objectToArray($subValue);
                }
                $objectArray[$key] = $subArr;

            } else {
                $objectArray[$key] = $reflectionMethod->invoke($object);
            }
        }

        return $objectArray;
    }
}