<?php

namespace App\Core;

use ReflectionClass;

class Factory
{

    public function createCollectionFromArray($entity, $data)
    {
        $reflection = new ReflectionClass($entity);
        $collection = [];
        foreach ($data as $userArray) {
            $collection[] = $this->getObject($userArray, $reflection);
        }

        return $collection;
    }

    private function getObject($array, $reflectionClass)
    {
        $entity = $reflectionClass->newInstance();
        foreach ($array as $column => $value) {
            $reflectionProp = $reflectionClass->getProperty($column);

            $reflectionProp->setAccessible(true);
            $reflectionProp->setValue($entity, $value);
            $reflectionProp->setAccessible(false);
        }

        return $entity;
    }
}
