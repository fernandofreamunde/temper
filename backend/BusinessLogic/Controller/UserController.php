<?php

namespace App\BusinessLogic\Controller;

use App\BusinessLogic\Service\UserService;
use App\Core\JsonResponse;

class UserController
{
    /**
     * @var UserService
     */
    private $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    public function indexAction()
    {
        return new JsonResponse(['data' => $this->userService->getRetentionCurveData()]);
    }
}