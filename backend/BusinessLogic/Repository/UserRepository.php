<?php

namespace App\BusinessLogic\Repository;

use App\BusinessLogic\Entity\User;
use App\Core\Configuration;
use App\Core\Factory;

class UserRepository
{
    /**
     * @var Configuration
     */
    private $configuration;
    /**
     * @var Factory
     */
    private $factory;

    public function __construct(Factory $factory, Configuration $configuration)
    {
        $this->configuration = $configuration;
        $this->factory = $factory;
    }

    /**
     * @return array
     */
    public function getUserOnboardingData()
    {
        $data = $this->getCsvData();
        return $this->hydrateObjects($data);
    }

    /**
     * @return array
     */
    private function getCsvData()
    {
        return array_map('str_getcsv', file($this->configuration->getDataDirectory().'/export.csv'));
    }

    /**
     * @param $data
     * @return array
     */
    private function hydrateObjects($data)
    {
        $csv = [];
        $fieldNames = [];
        $firstLine = true;
        foreach ($data as $csvLine) {
            $csvLine = $csvLine[0];
            $newLine = [];
            if ($firstLine) {
                $fieldNames = explode(';', $csvLine);
                $firstLine = false;
                continue;
            }

            $lineElements = explode(';',$csvLine);
            foreach ($lineElements as $key => $value) {
                $newLine[$fieldNames[$key]] = $value;
            }
            $csv[] = $newLine;
        }

        return $this->factory->createCollectionFromArray(User::class, $csv);
    }

}