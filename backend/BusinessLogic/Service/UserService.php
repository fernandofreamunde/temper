<?php

namespace App\BusinessLogic\Service;

use App\BusinessLogic\Entity\User;
use App\BusinessLogic\Repository\UserRepository;

class UserService
{
    /**
     * @var UserRepository
     */
    private $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function getRetentionCurveData(){
        $userData = $this->userRepository->getUserOnboardingData();
        $totalUsers = count($userData);

        $data = [];
        /** @var User $user */
        foreach ($userData as $user) {
            $data[$user->getOnboardingPerentage() == '' ? 0 : $user->getOnboardingPerentage()][] = $user;
        }

        krsort($data);

        $curveData = [];
        foreach ($data as $percentageKey => $values){

            $curveData[$percentageKey] = ceil(count($values) * 100 / $totalUsers);
        }

        return $curveData;
    }
}
