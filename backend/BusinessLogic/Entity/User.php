<?php

namespace App\BusinessLogic\Entity;

class User
{
    private $user_id;
    private $created_at;
    private $onboarding_perentage;
    private $count_applications;
    private $count_accepted_applications;

    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @return mixed
     */
    public function getOnboardingPerentage()
    {
        return $this->onboarding_perentage;
    }

    /**
     * @return mixed
     */
    public function getCountApplications()
    {
        return $this->count_applications;
    }

    /**
     * @return mixed
     */
    public function getCountAcceptedApplications()
    {
        return $this->count_accepted_applications;
    }
}