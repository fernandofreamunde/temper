# temper

This repo is an assessment for a job opening at Temper.

### Requirement

Temper has a complex, multi-step on-boarding process, due to this complexity they want to know in which step, represented as a percentage, users more often get stuck.
In order to do this they kindly provided the csv file in `backend/Data/` that has some info to work with.

#### Targets:

- Back-end in PHP producing JSON responses to be consumed by the front-end
- Clean, maintainable, and SOLID code
- Represent the data in web front-end in a chart using Highcharts
- At least 50% test coverage on business logic
- Clear setup instructions using nginx, php -S or artisan serve
- Upload the results to github/bitbucket (ensure the repo is publicly accessible)
- Supply a screenshot of the final result in the git repo
- Keep it simple; you should be able to finish the assessment within 4 hours

Bonus Points for:
- Use VueJS for front-end
- Use vanilla PHP on backend.

### Requirements

- Have docker and docker-compose correctly set up;
- Have npm installed;

### Steps to run the exercise

- clone the repo;
- in a terminal cd into `frontend/` and run `npm install`;
- in the same folder run `npm run build`;
- go back to the project root;
- run `docker-compose up`;
- open your browser and point it to: `http://localhost:81/`;
- a graph should be drawn in it;

The PHP side can be accessed on `http://localhost:8080/`.

### Disclaimer

Because I chose to do this from scratch I didin't think about how to configure the PhpUnit, so I didn't actually write tests.
If I was using Symfony the test suit would already be set up and so I would have the tests.

I gave up on the tests mainly because I already was taking too long in my view to deliver the assessment.

It also took me more than the 4 hours because I had to learn some stuff regarding docker, and re learn a bit of vue, since it is a bit rusty due to lack of use.

I didnt use the required tool for the charts, Highcharts required my email and I wont give it away for a single use. So I used Graph,js. In my view it is just another tool and should not be a deal breaker, maybe I'm wrong...

#### Screenshot

As per the requirements here is the image: 
![](screenshot.jpg)

I think it is safe to say that most users get stuck at the 40% step of the on-boarding flow.